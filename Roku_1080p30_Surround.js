/* eslint-disable */
function details() {
	return {
		id: "Roku_1080p30_Surround",
		Stage: "Pre-processing",
		Name: "H.264 MP4 Roku 1080p30 Surround, No Subs, No Title Meta, AAC & AC3",
		Type: "Video",
		Description: `[Contains built-in filter] This plugin removes subs, metadata (title only) and makes sure the video is h264 1080p mp4 with AAC and/or AC3 audio. \n\n`,
		Version: "1.00",
		Link: "https://bitbucket.org/thevadakin/tdarr_custom_plugins/src/master/Roku_1080p30_Surround.js",
		Tags: "pre-processing,handbrake,ffmpeg,h264",
		Inputs: [{
			name: 'webOptimize',
			tooltip: `Specify if any files should be checked for bitrate/size and reduced.
	                    \\nOptional.
	             \\nExample:\\n
	             true

	             \\nExample:\\n
	             false`,
		}],
	};
}

function plugin(file, librarySettings, inputs) {
	//Must return this object

	var response = {
		processFile: false,
		preset: "",
		container: ".mp4",
		handBrakeMode: false,
		FFmpegMode: false,
		reQueueAfter: false,
		infoLog: "",
	};

	//  Check if inputs.webOptimize have been left empty. If it has then exit plugin.
	if (inputs && inputs.webOptimize === '') {
		response.infoLog += '☒Plugin has not been configured, please configure required options. Skipping this plugin. \n';
		response.processFile = false;
		return response;
	}

	// check for video
	if (file.fileMedium !== "video") {
		console.log("File is not video");

		response.infoLog += "☒File is not video \n";
		response.processFile = false;

		return response;
	} else {

		// string of file data
		var jsonString = JSON.stringify(file);
		var ffmpegCommandInsert = "";
		var hasNonEngAudio = false;
		var needsAudioWork = false;
		var max_bitrate = 12500000; //12.5Mbps
		var aacIndex = {}; // {1: "aac", 2: "dts"}
		var needsAac = false;
		var ac3Index = []; // [{'index': 1, 'codec': 'dts', 'channels': 6}]
		var alternatebitrate = -1;
		var fileSizeGB = file.file_size / 1000;
		response.infoLog += `fileSizeGB: ${fileSizeGB}`;
		if ('DURATION-eng' in file.ffProbeData.streams[0].tags && file.ffProbeData.streams[0].tags['DURATION-eng'] !== undefined) {
			alternatebitrate = (file.file_size / (parseInt(file.ffProbeData.streams[0].tags['DURATION-eng'].split(':')[1]) * 0.0075)) * 1000;
		}


		// loop through streams
		var hasSubs = false;
		var audioStream = -1;
		for (var i = 0; i < file.ffProbeData.streams.length; i++) {
			try {
				// check for subs
				if (file.ffProbeData.streams[i].codec_type.toLowerCase() === "subtitle") {
					hasSubs = true;
				}
				// check for audio
				if (file.ffProbeData.streams[i].codec_type.toLowerCase() === "audio") {
					audioStream++;
					// check for non-english audio
					if ('tags' in file.ffProbeData.streams[i] && !file.ffProbeData.streams[i].tags.language
						.toLowerCase()
						.includes("eng") &&
						!file.ffProbeData.streams[i].tags.language
						.toLowerCase()
						.includes("und")
					) {
						hasNonEngAudio = true;
						ffmpegCommandInsert += `-map -0:a:${audioStream} `;
					} else {
						// check for 2 channel
						if (file.ffProbeData.streams[i].channels === 2) {
							aacIndex[audioStream] = file.ffProbeData.streams[i].codec_name.toLowerCase();
						} else {
							ac3Index.push({
								'index': audioStream,
								'codec': file.ffProbeData.streams[i].codec_name.toLowerCase(),
								'channels': file.ffProbeData.streams[i].channels
							});
						}
					}
				}
			} catch (err) {}
		}

		// check for non-h264
		if (file.ffProbeData.streams[0].codec_name !== "h264") {
			response.infoLog += "☒File is not h264 1080p \n";
			response.processFile = true;
			response.preset = '-Z "Roku 1080p30 Surround" ';
			if (JSON.stringify(aacIndex).toLowerCase().includes("aac")) {
				response.preset += '-E copy:* ';
			}
			if (inputs.webOptimize.toLowerCase() === 'true') {
				response.infoLog += 'Optimizing for web... \n';
				response.preset += ' -x vbv-maxrate=12000:vbv-bufsize=24000';
			}
			response.container = ".mp4";
			response.handBrakeMode = true;
			response.FFmpegMode = false;
			response.reQueueAfter = true;
			return response;
		} else {
			response.infoLog += "☑File is h264 1080p! \n";
		}

		// check for bitrate/filesize
		if (inputs.webOptimize.toLowerCase() === 'true') {
			if (file.ffProbeData.streams[0].bit_rate === undefined || file.ffProbeData.streams[0].bit_rate === 0) {
				if (alternatebitrate !== -1 && alternatebitrate > max_bitrate) {
					response.processFile = true;
					response.preset = '-Z "Roku 1080p30 Surround" -x vbv-maxrate=12000:vbv-bufsize=24000';
					if (JSON.stringify(aacIndex).toLowerCase().includes("aac")) {
						response.preset += '-E copy:* ';
					}
					response.container = ".mp4";
					response.handBrakeMode = true;
					response.FFmpegMode = false;
					response.reQueueAfter = true;
					response.infoLog += `☒File bitrate (${alternatebitrate / 1000}kbps) exceeds max bitrate (${max_bitrate / 1000}kbps)! \n`;
					return response;
				} else if (file.file_size / 1000 > 5) {
					response.processFile = true;
					response.preset = '-Z "Roku 1080p30 Surround" -x vbv-maxrate=12000:vbv-bufsize=24000';
					if (JSON.stringify(aacIndex).toLowerCase().includes("aac")) {
						response.preset += '-E copy:* ';
					}
					response.container = ".mp4";
					response.handBrakeMode = true;
					response.FFmpegMode = false;
					response.reQueueAfter = true;
					response.infoLog += `☒File size (${file.file_size / 1000}) is greater than 5GB! \n`;
					return response;
				} else {
					response.infoLog += `☑File size (${file.file_size / 1000}) is under 5GB! \n`;
					response.infoLog += `☑File bitrate (${alternatebitrate / 1000}kbps) is below max bitrate (${max_bitrate / 1000}kbps)! \n`;
				}
			} else {
				if (file.ffProbeData.streams[0].bit_rate > max_bitrate) {
					response.processFile = true;
					response.preset = '-Z "Roku 1080p30 Surround" -x vbv-maxrate=12000:vbv-bufsize=24000';
					if (JSON.stringify(aacIndex).toLowerCase().includes("aac")) {
						response.preset += '-E copy:* ';
					}
					response.container = ".mp4";
					response.handBrakeMode = true;
					response.FFmpegMode = false;
					response.reQueueAfter = true;
					response.infoLog += `☒File bitrate (${file.ffProbeData.streams[0].bit_rate / 1000}kbps) exceeds max bitrate (${max_bitrate / 1000}kbps)! \n`;
					return response;
				} else {
					response.infoLog += `☑File bitrate (${file.ffProbeData.streams[0].bit_rate / 1000}kbps) is less than max bitrate (${max_bitrate / 1000}kbps)! \n`;
				}
			}
		}

		// handle aac
		if (JSON.stringify(aacIndex).toLowerCase().includes("aac")) {
			response.infoLog += "☑File has aac track \n";
			var aacTrack = -1;
			for (track in aacIndex) {
				if (aacIndex[track] === "aac" && aacTrack < 0) {
					aacTrack = track;
					if (response.preset === "") {
						response.preset = ",-map 0:v -c copy ";
					}
					response.preset += `-map 0:a:${track} `;
				} else {
					response.infoLog += "☒File has extra 2 channel audio tracks \n";
					needsAudioWork = true;
					if (response.preset === "") {
						response.preset = ",-map 0:v -c copy ";
					}
					response.preset += `-map -0:a:${track} `;
				}
			}
		} else {
			response.infoLog += "☒File is missing aac track \n";
			needsAudioWork = true;
			needsAac = true;
			if (response.preset === "") {
				response.preset = ",-map 0:v -c:v copy ";
			}
			if (aacIndex && Object.keys(aacIndex).length > 0) {
				response.preset += `-map 0:a:${Object.keys(aacIndex)[0]} -c:a:${Object.keys(aacIndex)[0]} aac `;
				for (var i = 1; i < Object.keys(aacIndex).length; i++) {
					response.infoLog += "☒File has extra 2 channel tracks \n";
					response.preset += `-map -0:a:${Object.keys(aacIndex)[i]} `;
				}
			} else {
				response.preset += `-map 0:a:${ac3Index[0]['index']} -c:a:${ac3Index[0]['index']} aac `;
			}
		}
        
		// handle ac3
		if (ac3Index.length > 0) {
			// sort by highest channels, keep highest
			ac3Index.sort((a, b) => (a.channels > b.channels) ? -1 : 1);
			if (JSON.stringify(ac3Index).toLowerCase().includes("ac3")) {
				response.infoLog += "☑File has ac3 track \n";
				var ac3Track = -1;
				for (var i = 0; i < ac3Index.length; i++) {
					if (ac3Index[i]['codec'] === "ac3" && ac3Track < 0) {
						ac3Track = ac3Index[i]['index'];
						if (response.preset === "") {
							response.preset = ",-map 0:v -c copy ";
						}
						response.preset += `-map 0:a:${ac3Index[i]['index']} `;
					} else {
						response.infoLog += "☒File has extra surround audio tracks \n";
						needsAudioWork = true;
						if (response.preset === "") {
							response.preset = ",-map 0:v -c copy ";
						}
						response.preset += `-map -0:a:${ac3Index[i]['index']} `;
					}
				}
			} else {
				response.infoLog += "☒File is missing ac3 track \n";
				needsAudioWork = true;
				if (response.preset === "") {
					response.preset = ",-map 0:v -c:v copy ";
				}
				response.preset += `-map 0:a:${ac3Index[0]['index']} -c:a:${ac3Index[0]['index']} ac3 -ac:a:${ac3Index[0]['index']} ${ac3Index[0].channels} `;
				for (var i = 1; i < ac3Index.length; i++) {
					response.infoLog += "☒File has extra surround audio tracks \n";
					response.preset += `-map -0:a:${ac3Index[i]['index']} `;
				}
			}
		}

		// remove subs
		if (hasSubs) {
			needsAudioWork = true;
			if (response.preset === "") {
				response.preset = ",-map 0:v -c copy ";
			}
			response.preset += "-map -0:s ";
			response.infoLog += "☒File has subs \n";
		} else {
			response.infoLog += "☑File has no subs \n";
		}

		// remove title
		if (file.meta.Title !== undefined) {
			needsAudioWork = true;
			if (response.preset === "") {
				response.preset = ",-map 0 -c copy ";
			}
			response.preset += "-metadata title= ";
			response.infoLog += "☒File has title \n";
		} else {
			response.infoLog += "☑File has no title \n";
		}

		// remove non-english audio
		if (hasNonEngAudio) {
			needsAudioWork = true;
			if (response.preset === "") {
				response.preset = ",-map 0:v -c copy ";
			}
			response.preset += `${ffmpegCommandInsert} `;
			response.infoLog += "☒File has non-english audio \n";
		} else {
			response.infoLog += "☑File has english audio only \n";
		}


		// process subs, title, audio
		if (needsAudioWork) {
			response.preset += "-dn -map_metadata:c -1 ";
			response.processFile = true;
			response.container = ".mp4";
			response.handBrakeMode = false;
			response.FFmpegMode = true;
			response.reQueueAfter = true;
			return response;
		}

		// remux to mp4
		if (file.container != "mp4") {
			response.processFile = true;
			response.preset = ", -map 0 -c copy";
			response.container = ".mp4";
			response.handBrakeMode = false;
			response.FFmpegMode = true;
			response.reQueueAfter = true;
			response.infoLog += "☒File is not in mp4 container \n";
			return response;
		} else {
			response.infoLog += "☑File is in mp4 container \n";
		}

		response.processFile = false;
		response.infoLog += "☑File meets conditions \n";
		return response;
	}
}

module.exports.details = details;
module.exports.plugin = plugin;
